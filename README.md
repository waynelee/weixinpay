# weixinpay

#### 项目介绍
基于官方微信支付java sdk的封装，支持网页扫码支付，app支付，小程序支付

#### 安装教程

1. 此项目基于springboot1.5.10，确保本地有java环境，将项目下载或克隆至IDE。
2. 下载微信支付的证书，windows需要安装，linux上传即可。
3. 下载微信支付的sdk，直接运行maven install打入本地maven仓库，sdk源码中WXPayConfig.java可能不好用，可以将其修改为interface后再打入maven
```
public interface WXPayConfig {
	String getAppID();

	String getMchID();

	String getKey();

	InputStream getCertStream();

	int getHttpConnectTimeoutMs();

	int getHttpReadTimeoutMs();

	IWXPayDomain getWXPayDomain();

	boolean shouldAutoReport();

	int getReportWorkerNum();

	int getReportQueueMaxSize();

	int getReportBatchSize();
}
```

#### 使用说明

1. 配置 /weixinpay/src/main/java/com/yuyi/lwq/wxpay/PayConfig.java，证书路径一定要配置正确
2. 把代码放在自己项目里测试下，个人觉得官方的沙箱不好用。
