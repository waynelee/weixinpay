/**
 * 
 */
package com.yuyi.lwq.wxpay;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.springframework.util.StringUtils;

import com.github.wxpay.sdk.WXPay;

/**
 *
 * @author 李文庆
 * 2018年7月19日 下午7:47:40
 */
public class WXPayClient {
	
	/**统一下单，模式
	 * @param from 支付来源
	 * @param openid 小程序专用
	 * @param out_trade_no 订单号
	 * @param body 商品
	 * @param total_fee 金额（分）
	 * @param attach 自定义字段
	 * @return
	 * @throws Exception
	 */
	public static Map<String, String> getOrder(Integer from,String openid,String out_trade_no,String body, String total_fee,String attach) throws Exception {

		MyConfig config = new MyConfig();
		WXPay wxpay = null;
		//商户信息
		Map<String,String> param=new HashMap<String, String>();
		if (Objects.equals(from,2)) {
			//app支付
			param.put("trade_type","APP");//交易类型
			wxpay = new WXPay(config);
		}else if (Objects.equals(from,1)) {
			//网页扫码支付
			param.put("trade_type","NATIVE");//交易类型
			wxpay = new WXPay(config);
		}else if(Objects.equals(from,3)){
			if (!StringUtils.isEmpty(openid)) {
				//小程序支付
				param.put("trade_type","JSAPI");//交易类型
				param.put("openid",openid);
				MyJSConfig jsConfig = new MyJSConfig();
				wxpay = new WXPay(jsConfig);
			}
			
		}else{
			throw new RuntimeException("支付来源错误");
		}
		
		param.put("body",body);//商品描述
		param.put("out_trade_no",out_trade_no);//订单号
		param.put("total_fee",total_fee);//价格
		param.put("spbill_create_ip","127.0.0.1");//终端IP
		param.put("notify_url", PayConfig.NOTIFY_URL);//通知地址
		param.put("attach",attach);
		Map<String, String> resp = wxpay.unifiedOrder(param);
		
		return resp;
	}
	
	/**关闭订单
	 * @param out_trade_no
	 * @return
	 * @throws Exception
	 */
	public static Map<String, String> closeOrder(String out_trade_no) throws Exception{
		
		MyConfig config = new MyConfig();
		WXPay wxpay = new WXPay(config);
		//商户信息
		Map<String,String> param=new HashMap<String, String>();
		param.put("out_trade_no",out_trade_no);//订单号
		
		Map<String, String> resp = wxpay.closeOrder(param);
		
		return resp;
	}
	
	/**查询订单
	 * @param out_trade_no
	 * @return
	 * @throws Exception
	 */
	public static Map<String, String> queryOrder(String out_trade_no) throws Exception{
		
		MyConfig config = new MyConfig();
		WXPay wxpay = new WXPay(config);

		//商户信息
		Map<String,String> param=new HashMap<String, String>();
		param.put("out_trade_no",out_trade_no);//订单编号
        
		Map<String, String> resp = wxpay.orderQuery(param);
		
		return resp;
	}
	
	/**退款
	 * @param out_trade_no
	 * @param out_refund_no
	 * @param total_fee
	 * @param refund_fee
	 * @return
	 * @throws Exception
	 */
	public static Map<String, String> refund(String out_trade_no,String out_refund_no, String total_fee, String refund_fee) throws Exception{
		
		MyConfig config = new MyConfig();
		WXPay wxpay = new WXPay(config);

		//商户信息
		Map<String,String> param=new HashMap<String, String>();
		param.put("out_trade_no",out_trade_no);//订单编号
		param.put("out_refund_no",out_refund_no);//退款编号
		param.put("total_fee",total_fee);//订单金额
		param.put("refund_fee",refund_fee);//退款金额	
		param.put("notify_url", PayConfig.NOTIFY_URL);//通知地址
		
        Map<String, String> resp = wxpay.refund(param);
        
		return resp;
	}
	
	/**退款查询
	 * @param out_trade_no
	 * @return
	 * @throws Exception
	 */
	public static Map<String, String> refundQuery(String out_trade_no) throws Exception{
		
		MyConfig config = new MyConfig();
		WXPay wxpay = new WXPay(config);
		//商户信息
		Map<String,String> param=new HashMap<String, String>();
		param.put("out_trade_no",out_trade_no);//商户订单号
		
	    Map<String, String> resp = wxpay.refundQuery(param);
	        
	    return resp;
	}
	/**退款查询
	 * @param out_trade_no
	 * @return
	 * @throws Exception
	 */
	public static Map<String, String> refundQueryByRefund(String out_refund_no) throws Exception{
		
		MyConfig config = new MyConfig();
		WXPay wxpay = new WXPay(config);

		//商户信息
		Map<String,String> param=new HashMap<String, String>();
		param.put("out_refund_no",out_refund_no);//商户退款号
		
	    Map<String, String> resp = wxpay.refundQuery(param);
	        
	    return resp;
	}
	
	/**下载对账单
	 * @param bill_date
	 * @return
	 * @throws Exception
	 */
	public static Map<String, String> downloadBill (String bill_date) throws Exception{
		
		MyConfig config = new MyConfig();
	    WXPay wxpay = new WXPay(config);
	    
		//商户信息
		Map<String,String> param=new HashMap<String, String>();
		//param.put("nonce_str",WXPayUtil.generateNonceStr());//随机字符串
		param.put("bill_date",bill_date);
		param.put("bill_type","ALL");
		//param.put("tar_type", "GZIP");
		
		Map<String, String> resp = wxpay.downloadBill(param);
		
		return resp;
	}
}

