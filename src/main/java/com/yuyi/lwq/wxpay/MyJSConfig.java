/**
 * 
 */
package com.yuyi.lwq.wxpay;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import com.github.wxpay.sdk.IWXPayDomain;
import com.github.wxpay.sdk.WXPayConfig;

/**
 * 小程序支付的配置类
 * @author 李文庆
 * 2018年7月19日 下午7:42:37
 */
public class MyJSConfig implements WXPayConfig {
	
    private byte[] certData;

    public MyJSConfig() throws Exception {
    	
        String certPath = PayConfig.CERT_PARTH;
        File file = new File(certPath);
        InputStream certStream = new FileInputStream(file);
        this.certData = new byte[(int) file.length()];
        certStream.read(this.certData);
        certStream.close();
    }
	
	@Override
	public String getAppID() {
		
		return PayConfig.JS_APP_ID;
	}

	@Override
	public String getMchID() {
		
		return PayConfig.MCH_ID;
	}

	@Override
	public String getKey() {
		
		return PayConfig.KEY;
	}

	@Override
	public InputStream getCertStream() {
		
        ByteArrayInputStream certBis = new ByteArrayInputStream(this.certData);
        return certBis;
	}

	
	public int getHttpConnectTimeoutMs() {
		return 8000;
	}

	
	public int getHttpReadTimeoutMs() {
		return 10000;
	}

	
	public IWXPayDomain getWXPayDomain() {
		
		return WXPayDomainSimpleImpl.instance();
	}

	
	public boolean shouldAutoReport() {
		return false;
	}

	
	public int getReportWorkerNum() {
		return 6;
	}

	
	public int getReportQueueMaxSize() {
		return 10000;
	}

	
	public int getReportBatchSize() {
		return 10;
	}
}
