/**
 * 
 */
package com.yuyi.lwq.wxpay;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 *restTemplate的配置类
 * @author 李文庆
 * 2018年8月14日 下午5:00:04
 */
@Configuration
public class RestConfig {
	
	@Autowired
	private RestTemplateBuilder restTemplateBuilder;
	
	@Bean
	public RestTemplate restTemplate(){
		return restTemplateBuilder.build();
	}
}
