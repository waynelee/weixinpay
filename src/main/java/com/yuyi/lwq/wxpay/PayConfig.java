package com.yuyi.lwq.wxpay;

/**
 * 微信支付参数
 * @author 李文庆
 * 2018年5月17日 下午12:12:24
 */
public class PayConfig {

	/**扫码，app 公众账号ID	*/
	public static final String APP_ID = "wx..";
	
	/**商户号*/
	public static final String MCH_ID = "";
	
	/**API秘钥*/
	public static final String KEY = "..";
	
	/**小程序ID*/
	public static final String JS_APP_ID = "wx...";
	
	/**小程序秘钥，用于获取openid*/
	public static final String JS_APP_SECRET = "...";
	
	/***模式一回调URL*/
	public static final String NOTIFY_URL="https://api......";
	
	/**证书路径*/
	public static String CERT_PARTH = "E:\\wechatlicense\\cert01\\1516361741_20181106_cert.p12";
	/**linux系统下微信支付证书路径*/
	//public static String CERT_PARTH = "/var/ca/wppl/1516361741_20181106_cert.p12";
}
