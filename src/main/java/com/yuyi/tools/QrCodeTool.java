package com.yuyi.tools;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

public class QrCodeTool {  
	
	private static final int width = 300;
	private static final int height = 300;
	private static final String format = "png";
	
	/**生成简单的二维码
	 * @param content 要生成的文本内容
	 * @param picName 生成的二维码图片名称
	 * @param storePath 二维码图片的保存路径
	 * @return 二维码图片的保存路径
	 * @throws IOException
	 * @throws WriterException
	 */
	public static String simpleEncode(String content,String picName,String storePath) throws IOException, WriterException{
	   
	   String payCodeName = picName+LocalDateTimeTools.formatNow("yyyyMMddHHmmss");
	   
	   /**设置二维码参数*/
	   Map<EncodeHintType,Object> hints = new HashMap<EncodeHintType, Object>();
	   hints.put(EncodeHintType.CHARACTER_SET,"utf-8");    //指定字符编码为“utf-8”
       hints.put(EncodeHintType.ERROR_CORRECTION,ErrorCorrectionLevel.M);  //指定二维码的纠错等级为中级
       hints.put(EncodeHintType.MARGIN,1);    //设置图片的边距
       
       BitMatrix bitMatrix=new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, width, height,hints);
       Path file=new File(storePath+payCodeName+".png").toPath();
       MatrixToImageWriter.writeToPath(bitMatrix, format, file);
       
	   return storePath+payCodeName+".png";
	   
	}
}  
